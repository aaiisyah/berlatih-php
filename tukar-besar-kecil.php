<?php
function tukar_besar_kecil($string){
    //kode di sini
    $small = range('a','z');
    $capital = range('A', 'Z');
    for($i=0; $i<strlen($string); $i++){
        for($j=0; $j<26; $j++){
            if($string[$i]==$small[$j]){
                $string[$i] = $capital[$j];
            }elseif($string[$i]==$capital[$j]){
                $string[$i] = $small[$j];
            }else{
                $string[$i] = $string[$i];
            }
        }
    }
    echo $string . "<br>";

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>